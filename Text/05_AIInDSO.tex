\chapter{Možnosti využití metod umělé inteligence ve zvolené metodě}

V~rámci implementované metody byly testovány dvě odlišné techniky z~oblasti umělé inteligence. Tyto techniky se zaměřují na dílčí části původní metody. Konkrétně se jedná o~výběr zajímavých bodů v~rámci přípravy každého snímku a minimalizace chyby odhadu hrubého posuvu kamery. Tato kapitola popisuje použité techniky a jejich implementaci. 

\section{Minimalizace fotometrické chyby}
Minimalizace fotometrické chyby probíhá v~části \textit{určení pohybu kamery} popsané v~kapitole \ref{S:CamMove}. Tato minimalizace ovlivňuje určení polohy každého snímku a má přímý vliv na produkovanou kumulativní chybu, která je následně optimalizována ve snímku klíčovém. Je-li navíc tato chyba příliš velká, může vést ke snížení nalezeného počtu residuálních bodů. Nízký počet residuálních bodů může v~dalších snímcích způsobit další navýšení chyby určení polohy. Pro zvýšení spolehlivosti může být nutné v~takovýchto případech zvýšit počet klíčových snímků, jejichž zpracování je časově náročné. V~krajním případě může dojít i ke ztrátě kontextu a opětovné inicializaci za běhu. Ta se v~generovaném modelu obvykle projevuje jako velká jednorázová chyba. Vylepšení přesnosti tohoto odhadu může tedy v~důsledku vést k~rychlejšímu zpracování a především větší robustnosti, při zachování stejných konfiguračních parametrů, mezi které patří například množství generovaných bodů, nebo četnost klíčových snímků.

Původní metoda se v~této fázi orientuje především na vysokou rychlost zpracování. Nejprve je vygenerováno 31 odhadů na základě znalosti poslední známé polohy a obvyklého pohybu při natáčení videa. Tyto relativní předpokládané pohyby jsou pevně definované přímo v~kódu a jsou pro každé spuštění stejné. Předpokládá se, že právě testovaný odhad pohybu se nachází v~blízkosti skutečné polohy a že jeho nejmenší okolí, které obsahuje optimální polohu, má konvexní charakter. V~takovém případě je možné se vydat od současného odhadu ve směru zvyšujícího se gradientu. K~tomu původní algoritmus používá Gauss-Newtonovu metodu \ref{V:GausNewton}. Jedná se o~zobecnění Newtonovy metody \ref{V:Newton} do vícedimenzionálního prostoru. Geometrická představa Newtonovy metody odpovídá sestrojování tečen a hledání jejich průsečíků s~osou. Při zobecnění je druhá derivace nahrazena Hessovou maticí $H$ a derivace gradientem $\nabla$. Původní metoda navíc používá variantu s~možnou konfigurací velikosti kroku pomocí parametru $\gamma$ \ref{V:GausNewtonStepSize}. 

\begin{align}
x_{n+1}&=x_n - \dfrac{f\left(x_n\right)}{\dfrac{\delta f\left(x_n \right)}{\delta x}} =x_n - \dfrac{\dfrac{\delta f\left(x_n \right)}{\delta x}}{\dfrac{\delta^2 f\left(x_n \right)}{\delta x}} \label{V:Newton} \\
x_{n+1} &= x_n - \left[\mathrm{H} f\left(x_n \right) \right]^{-1} \nabla f\left(x_n\right), n \geq 0 \label{V:GausNewton}\\
x_{n+1} &= x_n - \gamma\left[\mathrm{H} f\left(x_n \right) \right]^{-1} \nabla f\left(x_n\right), n \geq 0 \label{V:GausNewtonStepSize}
\end{align}

Pokud výsledná chyba stanovená touto metodou překročí práh, je zkoušen další odhad v~pořadí.
Tato chyba může nabývat vysokých hodnot díky špatně zvolenému počátečnímu odhadu. Stejně tak se může ale zvýšit v~důsledku nesplněných předpokladů. Je-li například zaznamenán povrch s~mnoha opakujícími se vzory, může to mít negativní vliv na požadované vlastnosti stavového prostoru.
Je-li tak kontext již oslaben nízkým počtem residuálních bodů, může být tímto způsobem vybrán zcela chybný počáteční odhad pohybu kamery, a odtud pak plyne jednorázové navýšení chyby a nutnost opětovné inicializace. 

Původní metoda je také omezena fixním počtem kroků. Iterace je sice zastavena předčasně, pokud metoda dojde do lokálního optima a již neprovádí příliš velké změny. Opačný případ však kontrolován není. Není tedy zaručeno, že bude nalezeno lokální optimum, je zaručeno pouze to, že se původní odhad polohy vylepší.

Za účelem odstranění těchto problematických situací byl implementován genetický algoritmus pro upřesnění počátečního odhadu posuvu kamery. 

Genotyp jedince je zakódovaný relativní posuv a rotace kamery vůči právě testovanému odhadu.
\begin{align}
\overrightarrow{g}=\left(x,y,z,x_r,y_r,z_r\right)
\end{align}
První tři složky $\left(x,y,z\right)$ reprezentují translaci v~3D prostoru, zbylé tři složky $\left(x_r,y_r,z_r\right)$ reprezentují rotaci v~3D prostoru.

Počáteční populace jedinců je generována na základě prvního kroku Gauss-Newtonovy metody, díky které je získán první jedinec. Následně jsou náhodně generováni další jedinci populace z~okolí prvního jedince. Rozsah náhodných změn je omezen na $\pm400~\%$.

Selekce jedinců je řešena kvazináhodným výběrem, kdy je pravděpodobnost výběru jedince úměrná jeho zdatnosti.

Pro křížení je použita nejjednodušší jednobodová varianta s~náhodnou volbou hranic. Lze předpokládat, že výrazného zlepšení bude dosaženo buď změnou polohy, nebo změnou rotace. Křížení bylo voleno tak, aby existovala realistická šance, že bude jedna z~těchto trojic přenesena do potomka jako celek. Nově vytvoření potomci nahrazují nejméně zdatné jedince populace.

Dojde-li u~jedince k~mutaci, je náhodně vybrána jedna položka vektoru, která je následně náhodně změněna v~rozsahu $\pm400~\%$.

Pro každého jedince jsou následně spočteny residuální body a relativní fotometrická chyba. Tato chyba je vztažena k~počtu nalezených bodů a její převrácená hodnota slouží jako funkce zdatnosti jedince.

Protože výpočet zdatnosti populace je nejnáročnější částí algoritmu, je GA navržený tak, aby iterace skončila co možná nejdříve, a to i za cenu nalezení suboptimálního řešení. Z~tohoto důvodu byla výrazně redukována velikost populace na pouhých několik desítek jedinců a počet iterací byl také shora omezen.

Protože se populace výrazně zmenšila, byla zvýšena pravděpodobnost mutace z~původních nízkých jednotek procent na nízké desítky procent. Díky tomu dostal algoritmus příležitost opustit lokální atraktor i při nízkém počtu iterací.

\begin{figure}%\centering
\begin{minipage}[c]{.47\textwidth}
\includegraphics[width=\textwidth]{res/pointsOriginal}%
\end{minipage}
\hfill
$\rightarrow$
\hfill
\begin{minipage}[c]{.47\textwidth}
\includegraphics[width=\textwidth]{res/pointsMap}%
\end{minipage}
\caption[Ukázka vstupu a výstupu generátoru mapy kandidátních bodů]{Ukázka vstupu a výstupu generátoru mapy kandidátních bodů}\label{G:PointsMap}
\end{figure}

Touto změnou se však selekční tlak citelně zhoršil. Z~tohoto důvodu byl přidán mechanismus, který vliv mutace postupně snižoval tím, že dovolil jedince upravovat v~menším rozsahu. Tato míra snižování vlivu mutace byla stanovena na $97~\%$ původní hodnoty v~každé iteraci. Po 100 opakováních je tak možné jedince mutovat změnou pouhých $4~\%$ v~jedné dimenzi. V~důsledku se tak jedná o~kombinaci GA s~principem simulovaného žíhání.

Algoritmus také obsahuje mechanismus pro ponechání tří elitních jedinců v~populaci tak, aby nedošlo k~jejich změně, ale byli k~dispozici pro tvorbu potomků a mutaci. Tento mechanismus spolu s~možností vícenásobného výběru jednoho jedince zaručuje, že populace nezdegraduje. Tito elitní jedinci mohou být odstraněni pouze tak, že se v~populaci objeví jedinec, který je zdatnější.

\section{Generování bodů zájmu}
Výpočet gradientu se provádí pro každý snímek, výběr bodů potom pro všechny klíčové snímky a snímky inicializace. Každá možnost urychlení části, které se počítají pro všechny snímky, je časem znásobena. Z~tohoto důvodu se vyplatí využít každé možnosti urychlení této kritické části zpracování snímku.

Na základě informací získaných v~teoretické části byla implementována generativní konvoluční neuronová síť s~cílem urychlit generování kandidátních bodů.

Cílem je vytvořit z~originálního snímku mapu kandidátních bodů. Vstup a výstup zpracování, které má být nahrazeno neuronovou sítí, je znázorněno na obrázku \ref{G:PointsMap}.

Díky využití knihoven, které obsahují efektivní implementace trénovaní a evaluaci neuronových sítí, se problém implementace redukuje na:
\begin{itemize}
\item Určení struktury sítě. Rozhodnout, jaké vrstvy v~síti budou a proč.
\item Určení podoby jednotlivých vrstev. Rozhodnout, jaké budou použity aktivační funkce, jak budou voleny dimenze jednotlivých vrstev, nebo kdy a jak bude na jednotlivých vrstvách provedena normalizaci dat.
\item Určení ztrátové funkce, jejíž minimalizace povede blíže k~požadovaným výstupům.
\item Rozhodnutí, jaký algoritmus bude použit pro trénování sítě a jak bude trénování probíhat.
\end{itemize}
Obecně byly voleny konfigurace s~nižším počtem vrstev a parametrů, aby se zvýšila pravděpodobnost úspěšného trénování i při omezené výpočetní kapacitě. V~příkladech představených v~teoretické části bylo ukázáno, že podobné filtry je možné naučit i na relativně jednoduchých sítích, jsou-li dostatečně specializované.

Pro správné fungování zbylého programu jsou na generátor kandidátních bodů kladeny dva základní požadavky. Prvním je konzistence bodů. Je vyžadováno, aby generátor pro dva podobné snímky vybral body na odpovídajících místech. Nebude-li tento požadavek splněn, hledání odpovídajících si bodů na základě fotometrické chyby z~principu selže. Druhým požadavkem je rovnoměrné rozložení bodů na snímku. Tento požadavek existuje z~důvodu vyšší robustnosti programu. Pokud by se pozice kamery posunula tak, že by v~překryvu dvou sousedních snímků byla oblast s~malým výskytem hran, mohlo by se stát, že v~této překryvné části nebudou nalezeny odpovídající si body, protože všechny budou generovány v~oblastech s~vysokým gradientem.

První vrstvou sítě je samotný obrázek. Na vstupu se očekává snímek ve stupních šedi. Do sítě vstupuje bez jakéhokoliv dalšího předzpracování a jeho velikost přímo ovlivňuje dimensionalitu všech následujících vrstev.

První skryté vrstvy jsou konvoluční vrstvy, které propagují vstupní dimensionalitu na výstup. Jeli konvoluční jádro na kraji snímku, je část jádra mimo snímek vyplněna konstantou. U~této vrstvy se předpokládá, že se bude schopná naučit základní charakteristiky dané části snímku, jakými může být například míra přítomnosti hrany v~nějakém směru. Počet těchto vrstev je konfigurovatelný. Vyšší počet vrstev umožní využití širšího okolí konkrétního bodu. Třetí dimenze těchto vrstev, která bude dále nazývaná jako hloubka vrstvy, je také konfigurovatelná. Protože váhy zvoleného jádra jsou pro stejnou hloubku sdílené, větší hloubka vrstvy umožní síti naučit se více různých příznaků, každý generovaný jiným konvolučním jádrem (například hrany v~různých směrech). Počet těchto vrstev byl při testování volen spíše nižší, obvykle jediná vrstva, nebo nízké jednotky vrstev. Pro hloubky těchto vrstev pak byly voleny hodnoty $\left\{4,8,16\right\}$. Za předpokladu, že se v~těchto vrstvách budou vyskytovat hrany, byly tyto hodnoty odvozeny od počtu směrů, ve kterých by neuronová síť měla počítat gradient. V~originální metodě je pro výběr bodů vypočten gradient celkem v~16 různých směrech. 

Výstupem této části sítě je konvoluční vrstva, které je nastavována hloubka v~rozsahu $\left<1,2\right>$. U~této vrstvy se předpokládá, že již bude nějakým způsobem obsahovat zakódovanou informaci o~přítomnosti bodu ve výsledku. V~ideálním případě například pravděpodobnost výskytu, nebo příznak o~výskytu a váhu atp.

\gr[.75]{res/relu}{G:ReluFce}{RELU aktivační funkce}

Bylo také experimentováno s~paralelními vrstvami. Tyto vrstvy nejprve ve třech krocích velmi zredukovaly původní velikost snímku. Takto redukovaný snímek následně dekonvolucí opět zvětšily. Cílem těchto vrstev bylo dosáhnout propagace informace o~průměrném gradientu v~určitých regionech. Výsledný snímek byl následně použit pro vážení vrstvy pro detekci bodů. Díky tomu mělo dojít k~rovnoměrnějšímu rozložení bodů na snímku.

Výstupní vrstva již pouze redukuje hloubku poslední vrstvy na $1$, aby bylo možné výsledek převést do podoby bitmapy.

Jako aktivační funkce neuronů byla použita \gls{RELU} funkce, která je definovaná vzorcem \ref{V:ReLU}.

Tato funkce funguje překvapivě dobře pro učení hlubokých neuronových sítí. Někdy se používá hladká varianta $f_s$ \ref{V:ReLUS}, či tzv. \textit{Leaky RELU} $f_l$ \ref{V:ReLUL}. Jejich výhoda je především ve snazším a rychlejším trénování sítě. Běžně používané alternativy, jako například $tanh$, jsou často kromě úzkého intervalu hodnot velice ploché. Pro správné fungování se síť obvykle musí v~některých místech naučit využívat přesně tento úzký interval hodnot, což prodlužuje učení. V~některých vrstvách je využíváno funkce $tanh$ pro normalizaci hodnot do rozsahu $\left<0,1\right>$. Před vstupem hodnot do aktivační funkce jsou výstupní hodnoty neuronů normalizovány napříč dávkou několika snímků. Tato technika se používá k~urychlení učení právě díky vyšší pravděpodobnosti využití rozsahu hodnot, které mají největší vliv na rozhodování (v~případě RELU hodnoty kolem $0$, v~případě $tanh$ hodnoty v~rozsahu $\left<-2,2\right>$). V~případě vrstvy pro detekci bodů byly zkoušeny i varianty s~přidaným prahem, který učiní výstup striktně binární. Kompletní podoba jedné konfigurace popsané sítě je znázorněna na obrázku \ref{G:Architecture}.

\gr{res/nnStructure/nn}{G:Architecture}{Uspořádání vrstev neuronové sítě}

\begin{align}
f\left(x\right)&=max\left(0,x\right)\label{V:ReLU} \\
f_s\left(x\right)&=ln\left(1 + e^x\right)\label{V:ReLUS} \\
f_l\left(x\right)&=max\left(\alpha \cdot x,x\right)\label{V:ReLUL}
\end{align}
Ztrátová funkce se skládá ze dvou částí. Primární složkou je součet absolutních hodnot rozdílu mezi vygenerovanou a vzorovou mapou bodů \ref{V:diffLoss}. Aby bylo zabráněno ustálení na tmavém snímku, která má ve stanovené ztrátové funkci relativně nízké ohodnocení, byla přidána druhá část, která se neuronovou síť snaží přinutit vybrat co možná největší množství bodů \ref{V:whiteLoss}.

\begin{align}
\left| G \right| &= \left| O~\right| \\
f_{dLoss}\left(G,O\right)&= \sum_{i=1}^{\left| G \right|}{\left|g_i - o_i\right|} \label{V:diffLoss} \\
f_{wLoss}\left(G\right)&= 1 - \dfrac{\sum_{i=1}^{\left| G \right|}{g_i}}{\left| G \right|} \label{V:whiteLoss} \\
f_{Loss}\left(G,O\right)&= f_{dLoss}\left(G,O\right) + f_{wLoss}\left(G\right)
\end{align}
\begin{itemize}
\item {$G$} výsledná mapa bodů generována neuronovou sítí
\item {$O$} výsledná mapa bodů generována původní metodou
\item {$g_1,g_2, \dots ,g_{\left| G \right|}$} jednotlivé hodnoty snímku $G$
\item {$o_1,o_2, \dots ,o_{\left| O \right|}$} jednotlivé hodnoty snímku $O$
\end{itemize}

Primárně byly testovány dva učící algoritmy. Prvním je klasický \gls{SGD}. Druhou testovanou metodou je postup pojmenovaný autory jako \textit{Adam: A~Method for Stochastic Optimization}.

%TODO pokud by chyběly stránky možnost dopsat infor a způsobu učení pro NN např ADAM
