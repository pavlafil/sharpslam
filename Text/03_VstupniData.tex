\chapter{Vstupní data}

Vstupní data jsou omezena na sekvenci snímků. Snímek je definován jako perspektivní projekce okolí v~konkrétním čase a na konkrétní pozici v~prostoru. Formálně lze tuto sekvenci popsat jako:

\begin{align}
V&=\left(p_1, p_2, \dots, p_n \hspace{0.2em} \vert \hspace{0.2em} n=t \cdot h \cdot w \wedge p_i \in B \right) \\
B&=\left\{x \in \mathbb{N} \hspace{0.2em} \vert \hspace{0.2em} 0 \le x \le  255 \right\} \\
t &\in \mathbb{N}, h \in \mathbb{N},v \in \mathbb{N}
\label{V:VideDefinition}
\end{align}
\begin{itemize}
			\item []{$V$} videosekvence
			\item []{$t$} počet zaznamenaných snímků
			\item []{$h, w$} výška, šířka jednoho snímku
\end{itemize}
%\caption[Definice vstupních dat]{Definice vstupních dat}

Jedná se tedy o~běžné černobílé (jednokanálové) video, které bylo pořízeno kamerou s~jedním objektivem.

\section{Předzpracování dat}
V~metodách založených na deskriptorech, nebo podobně robustních vlastnostech obrazu, není obvykle tento krok příliš důležitý. V~přímých metodách, kde se pracuje se samotnými intenzitami, může  však chybné předzpracování způsobovat významné problémy, které se odvíjejí od zkreslení přítomného v~datech. Data uložená přímo záznamovým zařízením proto není obvykle možné přímo použít bez znalosti toho, jakým způsobem byla zaznamenána. Chceme-li tyto problémy odstranit, je nutné nejprve vstupní data sjednotit. 
 
 Zkreslení přítomná v~obraze mohou být způsobena například objektivem, nebo senzorem samotným a obvykle se pro každý vstup liší. Informace pro správné předzpracování obrazu je tedy nutné chápat jako součást vstupních dat.

\subsection{Geometrická korekce}
Geometrická korekce umožňuje sjednotit parametry vnitřní geometrie kamery, které definují projekci, k~níž došlo při pořízení snímku. Pro provedení geometrické korekce je nutné dodat informace o~kalibraci kamery. Tyto informace v~konečném důsledku definují funkci, která mapuje pixely původního obrazu na obraz nový, nezatížený zkreslením. Jde o~převod snímku z~původního modelu kamery do nového, dostatečně jednoduchého a jednotného modelu. Nový snímek popisuje, jak by projekce vypadala, pokud by byla vytvořena hypotetickou jednotnou kamerou.

Protože přesně popsat, k~jaké projekci původně došlo, bývá často velmi komplikované, obvykle je možné použít pouze zjednodušený model kamery. Nejčastěji používanými modely jsou:

\begin{figure}%\centering
\begin{minipage}[c]{.45\textwidth}
\includegraphics[width=\textwidth]{res/pinholeCamera}%
\end{minipage}
\hfill
\begin{minipage}[c]{.45\textwidth}
\includegraphics[width=\textwidth]{res/pinholeCameraModel}%
\end{minipage}    
\caption[Projekce dírkové kamery]{Projekce dírkové kamery}\label{G:PinholeCamera}
\end{figure}

\begin{itemize}
\item Dírková kamera \cite{pinholeModelBook}, \cite{pinholeModel}
\item FOV \cite{36}
\item KannalaBrandt \cite{34}, \cite{35}
\item RadTan \cite{35}, \cite{distModelsGitHub}, \cite{kalibrModelsGitHub}
\item EquiDistant \cite{35}, \cite{distModelsGitHub}, \cite{kalibrModelsGitHub}
\end{itemize}

Nejjednodušší a nejčastěji používaný model kamery je dírková kamera. Pro svou jednoduchost je často volena jako jednotný výstupní model. Jedná se o~popis perspektivní projekce za použití podobnosti trojúhelníků.

\begin{align}
g_{xo}\left( x_n \right) &=f_{xo} \dfrac{x_n - c_{xn}}{f_{xn}} + c_{xo} \\
g_{yo}\left( y_n \right) &=f_{yo} \dfrac{y_n - c_{yn}}{f_{yn}} + c_{yo}
\end{align}
\begin{itemize}
			\item []{$x_n, y_n$} souřadnice v~novém snímku
			\item []{$f_{xo}, f_{yo}$} ohnisková vzdálenost v~originálním modelu kamery, ve většině případů jsou obě ohniskové vzdálenosti stejné, různé ohniskové vzdálenosti umožňují pracovat s~obdélníkovými pixely
			\item []{$f_{xn}, f_{yn}$} ohnisková vzdálenost v~novém, jednotném modelu kamery
			\item []{$c_{xo}, c_{yo}$} určení bodu, kudy prochází optická osa v~originálním snímku (tzv. \textit{principal point})			
			\item []{$c_{xn}, c_{yn}$} určení bodu, kudy prochází optická osa v~novém snímku
			\item []{$g_{xo}, g_{yo}$} souřadnice v~originálním snímku
\end{itemize}

FOV model je velmi podobný modelu dírkové kamery. Obsahuje navíc parametr radiálního zkreslení, které je typické pro objektivy se značně širokým zorným polem. Uvedené vztahy se pro tento model změní následovně:

\begin{figure}%\centering
\begin{minipage}[c]{.45\textwidth}
\includegraphics[width=\textwidth]{res/radialDistorsionBefore}%
\end{minipage}
\hfill
\begin{minipage}[c]{.45\textwidth}
\includegraphics[width=\textwidth]{res/radialDistorsionAfter}%
\end{minipage}    
\caption[Příklad korekce radiálního zkreslení]{Příklad korekce radiálního zkreslení}\label{G:radialDistorsion}
\end{figure}

\begin{align}
g_{xo}\left( x_n \right) &=f_{xo} \cdot \dfrac{\arctg{ \left( r \cdot 2 \tg{\dfrac{d}{2}} \right)}}{d \cdot r} \cdot \dfrac{x_n - c_{xn}}{f_{xn}} + c_{xo} \\
g_{yo}\left( y_n \right) &=f_{yo} \cdot \dfrac{\arctg{ \left( r \cdot 2 \tg{\dfrac{d}{2}} \right)}}{d \cdot r} \cdot \dfrac{y_n - c_{yn}}{f_{yn}} + c_{yo} \\
r &= \sqrt{\left(\dfrac{x_n - c_{xn}}{f_{xn}}\right)^2 + \left(\dfrac{y_n - c_{yn}}{f_{yn}}\right)^2}
\label{V:FOVModel}
\end{align}
\begin{itemize}
	\item []{$d$} míra radiálního zkreslení
\end{itemize}

Ostatní uvedené typy modelů nejsou na použitých vstupních datech využity a proto je více nepřibližuji. Plnou definici jednotlivých modelů je možné dohledat v~následujících publikacích: \cite{pinholeModelBook}, \cite{pinholeModel}, \cite{36}, \cite{34}, \cite{35}, \cite{distModelsGitHub} a \cite{kalibrModelsGitHub}.

\subsection{Fotometrická korekce}
Fotometrická kalibrace je použita pro sjednocení jasové složky obrazu a je rozdělena do dvou fází. 

Pro korekci závislou na pozici pixelu v~obraze je použita tzv. viněta \cite{37}.

\begin{align}
f_{new}\left(t, x, y \right) &= \dfrac{f_V \left( t, x, y\right)}{f_{vignette}\left(x,y\right)}
\end{align}
\begin{itemize}
	\item []{$f_{new}$} intenzity nového snímku
	\item []{$f_{vignette}$} intenzita viněty na daných souřadnicích $x, y$, normalizovaná do rozsahu $\left<0,1\right>$
\end{itemize}

Tato korekce tedy zvýší intenzity v~těch částech snímku, ve kterých má viněta intenzity nízké, jak je znázorněno na obrázku \ref{G:Vignette}. Z~důvodu dalšího zpracování nejsou již intenzity uchovávány v~rozsahu $\left<0,255\right>$. Při vykreslení mezivýsledku tak dochází k~normalizaci intenzit v~rozsahu  $\left<0,\infty \right)$ do intervalu $\left<0,255\right>$. Protože snímek obsahuje v~některých částech velmi vysoké intenzity, je jeho vykreslení celkově tmavší oproti původnímu snímku i přes to, že došlo k~jeho zesvětlení. Tento jev vymizí, jakmile jsou ze snímku odstraněny zmíněné problematické části, k~čemuž dojde při geometrické korekci. Na obrázku \ref{G:radialDistorsion} je vidět, že ke ztmavení nedochází.

\begin{figure}%\centering
\begin{minipage}[c]{.30\textwidth}
\includegraphics[width=\textwidth]{res/vignetteOrig}%
\end{minipage}
\hfill
$\bullet$
\hfill
\begin{minipage}[c]{.30\textwidth}
\includegraphics[width=\textwidth]{res/vignetteVignette}%
\end{minipage}
\hfill
$=$
\hfill
\begin{minipage}[c]{.30\textwidth}
\includegraphics[width=\textwidth]{res/vignetteResult}%
\end{minipage}    
\caption[Fotometrická kalibrace -- viněta]{Fotometrická kalibrace -- viněta}\label{G:Vignette}
\end{figure}

Druhou fází fotometrické korekce je sjednocení intenzit zkreslených vždy stejným způsobem pro konkrétní rozsah. K~tomuto zkreslení může dojít například díky charakteristickým rysům senzoru, který je pro různé intenzity různě citlivý. Tato operace se nazývá gama korekce \cite{38}. Jako součást kalibrace kamery se proto dodává popis nelineárního mapování mezi intenzitami.
\begin{align}
 \gamma:B & \rightarrow B \\
 B &= \left\{x \in  \mathbb{R} \hspace{.2em} \vert \hspace{.2em} x \geq 0 \wedge x \leq 255\right\}
\end{align}


Třetím typem korekce, který se používá, ale není součástí samotného předzpracování, je sjednocení intenzit mezi snímky pořízených s~různým expozičním časem. Pro úspěšnou korekci je nutné dodat časy expozice společně s~pořízeným datasetem. Změna intenzity je prováděna pro celý snímek rovnoměrně v~poměru expozičních časů.

\begin{figure}%\centering
\begin{minipage}[c]{.2\textwidth}
\includegraphics[width=\textwidth]{res/gammaBefore}%
\end{minipage}
\hfill
$\bullet$
\hfill
\begin{minipage}[c]{.50\textwidth}
\includegraphics[width=\textwidth]{res/gammaGraph}%
\end{minipage}
\hfill
$=$
\hfill
\begin{minipage}[c]{.2\textwidth}
\includegraphics[width=\textwidth]{res/gammaAfter}%
\end{minipage}    
\caption[Fotometrická kalibrace -- gama]{Fotometrická kalibrace -- gama}\label{G:Vignette}
\end{figure}

\section{Veřejně dostupné datasety}
Díky velké oblíbenosti a dostupnosti použitého formátu lze jako vstup použít téměř jakékoliv video bez dodatečných úprav. Existují však desítky anotovaných datasetů, které kromě snímků samotných obsahují i velké množství dodatečných informací. Obvykle se jedná o~data z~gyroskopů a akcelerometrů, aktuální GPS pozici, nebo mapu okolí a skutečnou polohu vůči této mapě. Díky spolupráci příbuzných odvětví jsou ty největší a nejužívanější datasety vypsané a porovnané na jednom centrálním místě \cite{awesomeDatasets}.

\subsection{TUM dataset}

Dataset \cite{TUMDataset} vytvořený pro potřeby vizuální odometrie na technické univerzitě v~Mnichově -- \gls{TUM}. Tento dataset je používán jako hlavní dataset pro testovaní vybrané metody. Jedná se o~data, na kterých byl testován originální program, což usnadní vzájemné porovnání. Dále každá sekvence obsahuje informace pro plnou kalibraci kamery tak, jak je popsána v~předchozích kapitolách. To umožní redukovat problémy, které mohou vzniknout chybným předzpracováním a sjednocením snímků.
TUM~dataset má následující strukturu:

\vspace{.5em}
	\dirtree{%
		.1 {squence\_XX}\DTcomment{identifikátor konkrétní sekvence (části) datasetu}.
		.2 images\DTcomment{adresář obsahující snímky}.
		.3 00.jpg.		
		.3 01.jpg.		
		.3 02.jpg.
		.3 \dots.
		.2 camera.txt\DTcomment{informace pro geometrickou kalibraci kamery}.
		.2 groundtruthSync.txt\DTcomment{informace o~skutečné poloze kamery}.
		.2 pcalib.txt\DTcomment{fotometrická kalibrace -- definice gama korekce}.
		.2 statistics.txt.
		.2 times.txt\DTcomment{čas pořízení a doba expozice jednotlivých snímků}.
		.2 vignette.png\DTcomment{fotometrická kalibrace -- definice viněty}.
	}
	
\vspace{.5em}

Soubor $camera.txt$ popisuje vstupní a výstupní model kamery. Tyto modely jsou popsané v~následující, pro člověka čitelné, textové podobě:

\begin{lstlisting}[language={}, caption={Formát souboru camera.txt}, label={L:Camera.txt}]
Pinhole Fx Fy Cx Cy 0 | FOV Fx Fy Cx Cy d
šířka_originálního_snímku výška_originálníhosnímku
crop | full | none | "Fx Fy Cx Cy 0"
šířka_výstupního_snímku výška_výstupního_snímku
\end{lstlisting}

První dva řádky definují vstup. První popisuje model kamery, druhý pak velikost vstupního snímku. Poslední dva řádky definují totéž pro výstup. Speciální hodnoty $crop$, $full$, \dots umožňují vytvořit výstupní model automaticky. Jako výstupní model se v~naprosté většině případů používá model dírkové kamery (FOV model, kde $d=0$) \cite{TUMDatasetFormat}.

Dalšími důležitými soubory jsou $pcalib.txt$ a $vignette.png$, které obsahují informace pro fotometrickou kalibraci obrazu. $pcalib.txt$ obsahuje posloupnost čísel oddělených tabulátorem. Tato posloupnost začíná vždy hodnotou $0$ a končí hodnotou $255$. Gama funkce je pak na základě této posloupnosti definována takto:

\begin{align}
i_u\left(x\right)&=\left\lceil\dfrac{x \cdot \left\lvert P_f \right\rvert}{255}\right\rceil \\
i_l\left(x\right)&=\left\lfloor\dfrac{x \cdot \left\lvert P_f \right\rvert}{255}\right\rfloor \\
p_u\left(x\right)&=x-\left\lfloor x \right\rfloor \\
p_l\left(x\right)&=1-p_u\left(x\right) \\
\gamma\left(x\right)&= p_l\left(x\right)\cdot P_f\left(i_l\left(x\right)\right) + p_u\left(x\right)\cdot P_f\left(i_u\left(x\right)\right)
\end{align}
\begin{itemize}
	\item []{$P_f$} posloupnost získaná ze souboru
	\item []{$i_u, i_l$} indexy do posloupnosti sousedící s~$x$
	\item []{$p_u, p_l$} míra zastoupení sousedního indexu
	\item []{$\gamma$} výsledná gama funkce
\end{itemize}

Soubor $times.txt$ obsahuje na každém řádku identifikátor snímku, čas jeho pořízení a dobu expozice. Vše odděleno jednou mezerou. Každý řádek obsahuje informace o~jednom snímku.