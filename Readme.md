# SharpSLAM

> TODO

This project is heavily based on https://vision.in.tum.de/research/vslam/dso so maybe you want check this project first.

#### Looking for program itself? Navigate to [Programs/SharpSLAM](https://gitlab.fit.cvut.cz/pavlafil/sharpslamcode)

#### Looking for compiled program? Navigate to [ProgramsBuilds/SharpSLAM](https://gitlab.fit.cvut.cz/pavlafil/shaprslambuilds)

## Structure

 - **Bookbinding** Text cover
 - **Datasets** Input files (videos & camera configurations)
 - **ErrorTracking** Notes for debugging
 - **Notes** Other notes (important dates, ...)
 - **Programs** Both original and new program
 - **Research** Temporary - will be removed.
 - **SourceArticles** All used related articles in `pdf` & index & notes (not necessarily used in work itself)
 - **Text** Latex sources & build of document itself