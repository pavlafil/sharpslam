\chapter{Výběr metody}
Vzhledem k~množství existujících řešení by bylo nepraktické navrhovat kompletně novou metodu od začátku. Je tedy třeba vybrat vhodnou existující metodu, která má potenciál splnit cíle této práce a která je schopná adaptovat se dílčím změnám. Je také důležité, aby byly výstupy algoritmu snadno porovnatelné a měřitelné a aby výstup metody byl dostatečně informačně bohatý pro případné další zpracování do plnohodnotného 3D modelu.

Zvolenou metodou pro získání trojrozměrných informací o~prostředí je práce nazvaná \textit{Direct Sparse Odometry}. Jedná se o~metodu vizuální orometrie, pracující s~videem pořízeného kamerou s~jedním objektivem.

V~této kapitole popíši důvody, které vedly k~výběru právě této metody a přiblížím nejpodstatnější části zpracování snímků. Kapitola je určená spíše pro snazší pochopení algoritmu a nepopisuje veškeré detaily. Tato práce na vybraném algoritmu přímo závisí, proto doporučuji nejprve nastudovat původní publikaci \cite{27}. Kromě publikace samotné jsou veřejně k~dispozici i další informace, jako jsou zdrojové kódy nebo použité datasety \cite{dso}.

%Duvody použití
\section{Shrnutí metody}
DSO se řadí mezi SFM metody. Prvním krokem po předzpracování snímku je tedy extrakce vlastností z~obrazu. 
Autoři využívají přímý přístup, při kterém jsou používány intenzity snímku, nikoli již předzpracované deskriptory. Tato vlastnost může být z~pohledu AI metod velmi významná, protože umožňuje použití oblíbených konvolučních sítí. K~dalšímu zpracování nejsou zpravidla použity všechny dostupné intenzity, ale pouze vybraná místa, která mají potenciál být z~pohledu dalšího zpracování zajímavá. Množství použitých bodů je možné konfigurovat. Na základě momentální konfigurace se tak algoritmus řadí se mezi tzv. \textit{sparse} až \textit{semi-dense} metody. Díky omezenému počtu bodů může v~původní metodě docházet k~výraznému urychlení, a není proto nutné používat velmi výkonný, nebo specializovaný hardware. Tento způsob předzpracování však není zcela běžný pro práci s~konvolučními sítěmi, kde se obvykle filtrování redundantních informací ponechává na učícím algoritmu a síti samotné.

Určení pohybu kamery je založeno na minimalizaci fotometrické chyby mezi odpovídajícími si body napříč několika snímky plovoucího okénka. Plovoucí okénko se skládá z~fixního počtu klíčových snímků. S~každým novým klíčovým snímkem je jeden starý odstraněn. Mezi každými sousedními klíčovými snímky je několik běžných snímků. Tato mezera mezi klíčovými snímky opět přispívá k~urychlení algoritmu.

Zde se otevírá více teoretických možností pro využití technik z~prohledávání stavového prostoru, ať už pro optimalizace chyby samotné, nebo pro optimální nastavení vstupních parametrů (počet klíčových snímků, mezera mezi klíčovými snímky, počet vybraných bodů, \dots).

Výstupem optimalizace je pak poloha a orientace kamery v~prostoru a hloubková mapa vybraných bodů.

\gr[1]{res/dsoDiagram/pdf}{G:DsoDiagram}{Průchod dílčími částmi DSO algoritmu}

V~následujících kapitolách jsou jednotlivé části algoritmu blíže popsány. Průchod těmito částmi je znázorněn na diagramu \ref{G:DsoDiagram}. Při běhu programu se tento průchod obvykle mění následujícím způsobem: 
\begin{itemize}
\item{Pro prvních několik snímků se program drží v~cyklu \textit{Příprava snímku} $\rightarrow$ \textit{Inicializace}. }
\item {Následně proběhne jednou cyklus \textit{Příprava snímku} $\rightarrow$ \textit{Inicializace} $\rightarrow$ \textit{Zpracování klíčového snímku}, čímž je ukončena inicializace.}
\item{Poté se opakuje cyklus \textit{Příprava snímku} $\rightarrow$ \textit{Určení polohy kamery} $\rightarrow$ \textit{Zpracování neklíčového snímku}}.
\item {V~relativně pravidelných intervalech je mezi zpracování obyčejných snímku vložen jeden cyklus \textit{Příprava snímku} $\rightarrow$ \textit{Určení polohy kamery} $\rightarrow$ \textit{Zpracování klíčového snímku}.}
\end{itemize}



\section{Příprava snímku}
\label{S:FramePrepare}
Zpracování začíná vytvořením několika zmenšených variant snímku. Další kopie má vždy poloviční rozměry. Vytváření kopií pokračuje, dokud má snímek sudé velikosti stran, nebylo dosaženo maximálního počtů kopií (z~konfigurace~--~obvykle $\pm$\hspace{1pt}6 vrstev) a počet pixelů zmenšovaného obrázku je alespoň 5000. Tyto kopie se vytvářejí pro urychlení některých částí dalšího zpracování.

Pro všechny tyto snímky jsou následně vypočteny derivace ve svislé $\left(\frac{\delta f}{\delta y}\right)$ a vodorovné $\left(\frac{\delta f}{\delta x}\right)$ ose, stejně jako jejich kombinace  $\left(\left(\frac{\delta f}{\delta x}\right)^2+\left(\frac{\delta f}{\delta y}\right)^2\right)$. Jedná se o~standardní detekci hran. Grafické znázornění hran v~obou směrech je na obrázku \ref{G:Gradient}. Tyto informace jsou dále použity k~určení potencionálně zajímavých bodů.

\begin{figure}
\centering
%\begin{minipage}[c]{.4\textwidth}
\includegraphics[width=.4\textwidth]{res/grad_0}\hspace{2pt}\includegraphics[width=.4\textwidth]{res/grad_1}\vspace{2pt}
\includegraphics[width=.4\textwidth]{res/grad_2}\hspace{2pt}\includegraphics[width=.4\textwidth]{res/grad_3}
%\end{minipage}   
\caption[Výpočet gradientu pro různé velikosti snímku]{Výpočet gradientu pro různé velikosti snímku}\label{G:Gradient}
\end{figure}



\section{Inicializace}
Prvním krokem inicializace je vytvoření bodů. Algoritmus si v~pozdějších fázích drží fixní počet bodu $N_p$ napříč všemi klíčovými snímky. Toto číslo je konfigurovatelné. Jako rozumný kompromis mezi rychlostí a robustností algoritmu byla pro většinu testování použita hodnota doporučená autory 2000 bodů. Protože body jsou nakonec vybírány dle toho, jak dobře je lze sledovat mezi snímky, v~rámci inicializace jsou vytvořeni pouze tzv. kandidáti. Pro každý snímek je vytvořeno $N_p$ kandidátů, čímž je zajištěn dostatečný přebytek kandidátů pro tvorbu bodů.
 
Cílem je vytvořit takové body, které jsou na snímku rovnoměrně rozloženy a mají lokálně vysoký gradient. Snímek je nejprve rozdělen do bloku $32\times32$ pixelů a je spočten lokální práh. Obrázek je následně rozdělen na stejně velké části. Z~každé části je vybrán bod, s~největším gradientem, který přesáhne práh v~dané oblasti. Pokud žádný takový bod neexistuje, bod přidán není. Tento postup se opakuje několikrát se snižujícím se prahem, aby bylo zajištěno rovnoměrné pokrytí i v~případě malého výskytu hran. Algoritmus tedy rovnoměrně vybírá konstantní počet ($N_p$) bodů a přednostně vybírá ty, které se vyskytují na místě s~vysokým gradientem.

Následně probíhá hledání tzv. reziduálních bodů. Jedná se o~reprezentaci bodu na jiném snímku, než na kterém byl původně vytvořen. Algoritmus postupně upravuje vzájemnou pozici dvou sousedních snímků, hledá residuální body a jejich hloubku ve snímku. Jedná se o~iterativní proces, který končí, pokud došlo k~příliš velké změně polohy, proběhl maximální počet iterací, nebo se počet residuálních bodů začal v~několika posledních iteracích snižovat. Protože se v~konečném důsledku jedná o~řešení mnoha soustav lineárních rovnic (viz. původní publikace \cite{27}), což je výpočetně nejnáročnější část této části, probíhá výpočet nejprve na zmenšených snímcích. Pokud hledání uspěje na zmenšeném a rozmazaném snímku, jsou nalezené body propagovány do většího snímku a celý proces se opakuje.

Jakmile jsou nalezeny residuální body pro současný snímek, vše probíhá znovu pro snímek nový. Inicializace je ukončena, jakmile je nalezeno vyhovující množství dostatečně kvalitních residuálních bodů.
Funkcionál, který určuje kvalitu, je přímo závislý na počtu nalezených bodů, velikost vzájemné translace snímků a hloubce residuálních bodů. Konkrétní práh je relativní vzhledem k~počtu nalezených bodů.

Poté je z~celkového množství kandidátů náhodně vybráno pouze $N_p$ bodů (označovaných jako \textit{ImmaturePoint}). Tyto body slouží jako kandidáti po dokončení inicializace. \textit{ImmaturePoint} je první stádium každého bodu, ze kterého se může, ale také nemusí stát bod klíčového snímku. Přestože se v~původním programu nejedná zcela o~totéž, budu tyto body nadále označovat jako kandidáty, protože plní stejnou úlohu. Označení by nemělo být matoucí, protože inicializace a další zpracování jsou striktně oddělené a s~původním významem kandidátů se již více nesetkáme. 

První a poslední snímek inicializace je označen jako klíčový. Tím inicializace končí. Poslední snímek je již zpracován jako každý jiný klíčový snímek tak, jak bude popsáno v~kapitole \ref{S:KeyFrame}.

\section{Určení pohybu kamery}
\label{S:CamMove}
Proběhla-li již jednou inicializace v~pořádku, bude pro další zpracovávaný snímek provedena pouze jeho příprava \ref{S:FramePrepare} a následně se již pokračuje určením pohybu kamery. 

Protože tato část zpracování probíhá pro každý snímek, je zde opět využito zmenšených kopií a v~mnoha směrech se podobá inicializaci. V~této fázi jde především o~co nejrychlejší nalezení přibližné správné polohy. Předpokládá se, že mezi sousedními snímky došlo pouze k~malé změně polohy a rotace. Jsou tedy vygenerovány potencionálně možné změny. Tyto změny jsou obvykle odvozeny od posledního pohybu. Zkouší se například stejný pohyb jako u~minulého snímku, dvojnásobný a poloviční pohyb, zda nedošlo k~žádnému pohybu vůči poslednímu snímku nebo vůči poslednímu klíčovému snímku. Vyzkoušeny jsou také jednoduché rotace všemi směry. Každý takovýto předpokládaný pohyb je následně otestován. Začíná se opět od nejmenší kopie snímku a postupuje se k~těm podrobnějším a přesnějším tak, aby bylo zajištěno, že výrazně špatné pohyby budou brzy a rychle vyřazeny. Testování každého pohybu je velmi podobné inicializaci. Jsou nalezeny reziduální body v~posledním klíčovém snímku a pohyb se iterativně upravuje tak aby se minimalizovala fotometrická chyba mezi snímky. Následně je ze všech pokusů vybrán nejlepší kandidát.

Po hrubém odhadu pozice je rozhodnuto, zda se bude snímek označen jako klíčový. Jako klíčový je označen snímek ve kterém došlo k~velké celkové změně jasu, odhad pozice snímku se příliš liší od předchozího snímku, nebo fotometrická chyba snímku dosáhla dvojnásobku chyby prvního neklíčového snímku po posledním klíčovém snímku.

Podle toho, zda je snímek klíčový, se provede příslušné zpracování, jak je popsáno v~kapitolách \ref{S:NotKeyFrame} a \ref{S:KeyFrame}.

\section{Zpracování neklíčového snímku}
\label{S:NotKeyFrame}

V~tomto kroku jsou pouze dohledány residuální body kandidátů ve všech klíčových snímcích, které jsou součástí plovoucího okénka. Pozice bodů je určena na základě pozice bodů v~prostoru (kombinace pozice snímku a hloubky bodu vůči snímku). Všichni kandidáti, pro které vyjde alespoň jeden residuální bod mimo zorné pole snímku, nebo je na okraji snímku, jsou označeny jako \gls{OOB}, címž jsou efektivně odstraněny z~dalších výpočtů. Pozice residuálních bodů je pak individuálně lokálně upřesněna Gaus-Newtonovou metodou (viz. původní práce \cite{27}). Na závěr je zveřejněna aktuální pozice kamery.


\section{Zpracování klíčového snímku a rekonstrukce scény}
\label{S:KeyFrame}
Zpracování klíčového snímku začíná naprosto stejným způsobem jako zpracování neklíčového snímku \ref{S:NotKeyFrame}. Pro každý bod z~kandidátů jsou spočteny všechny residuální body, nebo je kandidát odstraněn, resp. označen jako OOB.

Protože se jedná o~nový klíčový snímek, bude přidán do plovoucího okénka. První operací je proto marginalizace starých snímků. K~odstranění klíčových snímků z~plovoucího okénka jsou označeny takové snímky, které obsahují malý počet bodů (obvykle protože body z~tohoto snímku již nejsou v~záběru ostatních klíčových snímků). Pokud po této redukci existuje stále příliš mnoho snímků (maximální počet je určen konfigurací), jsou postupně odstraňovány snímky, které jsou od ostatních nejvíce vzdálené. Jakmile je v~plovoucím okénku místo pro nový snímek, je přidán.

Protože byl přidán nový snímek, je třeba přidat residuální body všem bodům ostatních snímků. Jinými slovy dohledat pozice všech již existujících bodů v~nově přidaném snímku.

Následně jsou pro nový snímek aktivovány body. Během aktivace se z~kandidátů stávají skutečné body daného snímku. Je dopočítána jejích hloubka (jak daleko jsou od projekční roviny). Přidány jsou všechny body, které nemají nastavený žádný příznak závadnosti (např. že již nejsou v~zorném poli -- OOB) a které mají alespoň nějakou minimální vzdálenost (opět práh získaný z~konfigurace). Díky nastavení minimální hloubky jsou odfiltrovány body, které by se vykreslily v~bezprostřední blízkosti kamery nebo v~ní. S~největší pravděpodobností se jedná o~chyby.

Jakmile mají všechny snímky své body a každý bod dohledaný residuální body v~cizích snímcích, jsou všechny residuální body (body napříč všemi klíčovými snímky okénka) optimalizovány. Tzn. vzájemná poloha snímku je upravena tak, aby fotometrická chyba mezi body a jejich residuálními body byla co nejmenší.

Protože znovu došlo ke změně pozic snímků, jsou opět odstraněny body, kterým již byly odebrány všechny residuální body. Všechny body a snímky, které doposud byly označovány jako \textit{k~odstranění}, jsou v~této fázi skutečně odstraněny. Teprve teď proběhne také marginalizace snímku (i když snímek k~marginalizaci byl vybrán na samém počátku).

Poté jsou již uloženy pozice snímků a prostředí se připraví pro zpracování snímku dalšího. To obnáší především:

\begin{itemize}
\item{Vytvoření nových kandidátů na body (aby byl zajištěn přibližně konstantní počet bodů $N_p$). Proces vytváření kandidátů se neliší od toho který je použit při inicializaci.}
\item {Zveřejnění nalezených bodů a jejich hloubek}
\item {Zveřejnění aktuální pozice kamery}
\end{itemize}
