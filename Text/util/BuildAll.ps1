﻿$fileName="DP_Pavlas_Filip_2019"
.\Spaces.ps1
cd ..
pdflatex -synctex=1 -interaction=nonstopmode "$fileName.tex" -aux-directory=build -output-directory=build
makeindex "build/$fileName.idx"
bibtex "build/$fileName"
cd build
makeglossaries "$fileName"
cd ..
pdflatex -synctex=1 -interaction=nonstopmode "$fileName.tex" -aux-directory=build -output-directory=build
pdflatex -synctex=1 -interaction=nonstopmode "$fileName.tex" -aux-directory=build -output-directory=build
cd util
