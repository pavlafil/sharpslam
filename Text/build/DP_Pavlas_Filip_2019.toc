\babel@toc {czech}{}
\babel@toc {czech}{}
\contentsline {subsection}{Odkaz na tuto pr{\' a}ci}{vi}{section*.1}% 
\contentsline {chapter}{{\' U}vod}{1}{chapter*.5}% 
\contentsline {section}{C\IeC {\'\i }l pr\IeC {\'a}ce}{1}{section*.6}% 
\contentsline {chapter}{\chapternumberline {1}Metody generov\IeC {\'a}n\IeC {\'\i } model\IeC {\r u}}{3}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Generov\IeC {\'a}n\IeC {\'\i } s~omezen\IeC {\'\i }m}{3}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Generov\IeC {\'a}n\IeC {\'\i } bez omezen\IeC {\'\i }}{3}{section.1.2}% 
\contentsline {chapter}{\chapternumberline {2}Vyu\IeC {\v z}it\IeC {\'\i } metod um\IeC {\v e}l\IeC {\'e} inteligence pro generov\IeC {\'a}n\IeC {\'\i } 3D model\IeC {\r u}}{7}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Metody prohled\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } stavov\IeC {\'e}ho prostoru}{7}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Gradientn\IeC {\'\i } algoritmus}{8}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}Simulovan\IeC {\'e} \IeC {\v z}\IeC {\'\i }h\IeC {\'a}n\IeC {\'\i }}{8}{subsection.2.1.2}% 
\contentsline {subsection}{\numberline {2.1.3}Genetick\IeC {\'y} algoritmus}{8}{subsection.2.1.3}% 
\contentsline {section}{\numberline {2.2}Metody zpracov\IeC {\'a}n\IeC {\'\i } obrazu}{11}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Neuronov\IeC {\'e} s\IeC {\'\i }t\IeC {\v e}}{12}{subsection.2.2.1}% 
\contentsline {chapter}{\chapternumberline {3}Vstupn\IeC {\'\i } data}{15}{chapter.3}% 
\contentsline {section}{\numberline {3.1}P\IeC {\v r}edzpracov\IeC {\'a}n\IeC {\'\i } dat}{15}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Geometrick\IeC {\'a} korekce}{16}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.1.2}Fotometrick\IeC {\'a} korekce}{18}{subsection.3.1.2}% 
\contentsline {section}{\numberline {3.2}Ve\IeC {\v r}ejn\IeC {\v e} dostupn\IeC {\'e} datasety}{19}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}TUM dataset}{19}{subsection.3.2.1}% 
\contentsline {chapter}{\chapternumberline {4}V\IeC {\'y}b\IeC {\v e}r metody}{21}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Shrnut\IeC {\'\i } metody}{21}{section.4.1}% 
\contentsline {section}{\numberline {4.2}P\IeC {\v r}\IeC {\'\i }prava sn\IeC {\'\i }mku}{23}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Inicializace}{23}{section.4.3}% 
\contentsline {section}{\numberline {4.4}Ur\IeC {\v c}en\IeC {\'\i } pohybu kamery}{24}{section.4.4}% 
\contentsline {section}{\numberline {4.5}Zpracov\IeC {\'a}n\IeC {\'\i } nekl\IeC {\'\i }\IeC {\v c}ov\IeC {\'e}ho sn\IeC {\'\i }mku}{25}{section.4.5}% 
\contentsline {section}{\numberline {4.6}Zpracov\IeC {\'a}n\IeC {\'\i } kl\IeC {\'\i }\IeC {\v c}ov\IeC {\'e}ho sn\IeC {\'\i }mku a rekonstrukce sc\IeC {\'e}ny}{25}{section.4.6}% 
\contentsline {chapter}{\chapternumberline {5}Mo\IeC {\v z}nosti vyu\IeC {\v z}it\IeC {\'\i } metod um\IeC {\v e}l\IeC {\'e} inteligence ve zvolen\IeC {\'e} metod\IeC {\v e}}{27}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Minimalizace fotometrick\IeC {\'e} chyby}{27}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Generov\IeC {\'a}n\IeC {\'\i } bod\IeC {\r u} z\IeC {\'a}jmu}{30}{section.5.2}% 
\contentsline {chapter}{\chapternumberline {6}Implementace}{35}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Pou\IeC {\v z}it\IeC {\'e} technologie}{35}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Pou\IeC {\v z}it\IeC {\'e} knihovny}{36}{section.6.2}% 
\contentsline {subsection}{\numberline {6.2.1}Newtonsoft.Json}{36}{subsection.6.2.1}% 
\contentsline {subsection}{\numberline {6.2.2}System.Drawing.Common}{36}{subsection.6.2.2}% 
\contentsline {subsection}{\numberline {6.2.3}HelixToolkit.Wpf}{36}{subsection.6.2.3}% 
\contentsline {subsection}{\numberline {6.2.4}CNTK.CPUOnly}{37}{subsection.6.2.4}% 
\contentsline {subsection}{\numberline {6.2.5}Supercluster.KDTree}{37}{subsection.6.2.5}% 
\contentsline {subsection}{\numberline {6.2.6}Eigen}{37}{subsection.6.2.6}% 
\contentsline {subsection}{\numberline {6.2.7}Sophus}{37}{subsection.6.2.7}% 
\contentsline {section}{\numberline {6.3}P\IeC {\v r}enositelnost}{37}{section.6.3}% 
\contentsline {section}{\numberline {6.4}Struktura}{38}{section.6.4}% 
\contentsline {section}{\numberline {6.5}Mo\IeC {\v z}nosti kompilace}{40}{section.6.5}% 
\contentsline {chapter}{\chapternumberline {7}V\IeC {\'y}sledky}{41}{chapter.7}% 
\contentsline {section}{\numberline {7.1}Implementace metody}{41}{section.7.1}% 
\contentsline {subsection}{\numberline {7.1.1}P\IeC {\v r}edzpracov\IeC {\'a}n\IeC {\'\i } obrazu}{41}{subsection.7.1.1}% 
\contentsline {subsection}{\numberline {7.1.2}V\IeC {\'y}stupn\IeC {\'\i } vrstva}{42}{subsection.7.1.2}% 
\contentsline {subsection}{\numberline {7.1.3}Kvalitativn\IeC {\'\i } vlastnosti j\IeC {\'a}dra metody}{42}{subsection.7.1.3}% 
\contentsline {section}{\numberline {7.2}Minimalizace fotometrick\IeC {\'e} chyby}{44}{section.7.2}% 
\contentsline {section}{\numberline {7.3}Generov\IeC {\'a}n\IeC {\'\i } bod\IeC {\r u} z\IeC {\'a}jmu}{45}{section.7.3}% 
\contentsline {section}{\numberline {7.4}Export modelu a ov\IeC {\v e}\IeC {\v r}en\IeC {\'\i } jeho spr\IeC {\'a}vnosti}{46}{section.7.4}% 
\contentsline {chapter}{Z{\' a}v{\v e}r}{49}{chapter*.7}% 
\contentsline {chapter}{Literatura}{51}{chapter*.8}% 
\contentsline {appendix}{\chapternumberline {A}Seznam pou{\v z}it{\' y}ch zkratek}{55}{appendix.A}% 
\contentsline {appendix}{\chapternumberline {B}Porovn\IeC {\'a}n\IeC {\'\i } s~origin\IeC {\'a}ln\IeC {\'\i } implementac\IeC {\'\i }}{57}{appendix.B}% 
\contentsline {appendix}{\chapternumberline {C}Minimalizace fotometrick\IeC {\'e} chyby}{61}{appendix.C}% 
\contentsline {appendix}{\chapternumberline {D}Generov\IeC {\'a}n\IeC {\'\i } bod\IeC {\r u} z\IeC {\'a}jmu}{69}{appendix.D}% 
\contentsline {appendix}{\chapternumberline {E}Export model\IeC {\r u} do virtu\IeC {\'a}ln\IeC {\'\i } reality}{73}{appendix.E}% 
\contentsline {appendix}{\chapternumberline {F}Obsah p\IeC {\v r}ilo\IeC {\v z}en\IeC {\'e}ho m\IeC {\'e}dia}{77}{appendix.F}% 
