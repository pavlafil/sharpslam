\chapter{Využití metod umělé inteligence pro generování 3D modelů}
Naprostá většina kompletních AI technik pro generování modelů, je založena na myšlence generování s~omezením. Trénovací množina dat je v~takových případech dostatečně sémanticky omezena, aby bylo možné získat požadované výstupy. Jinými slovy je dané metodě ponechána dostatečná volnost, aby si vytvořila abstraktní představu o~daném modelu nebo prostředí, kterou následně použije pro samotné generování. Tato vlastnost je pro některé AI metody typická a je hlavním důvodem proč se používají. Díky tomu však obvykle postrádají dostatečnou přesnost a robustnost, což jsou hlavní důvody proč bylo v~této práci od těchto kompletních řešení upuštěno. Jejich potenciál je spíše v~generování zcela nových (neznámých) modelů, nebo dodatečném zpracování například při začišťování modelu, nebo pro dogenerování chybějících částí. Pro vygenerování základního modelu ze sekvence fotografií jsou stále přesnější konvenční metody popsané v~předchozích kapitolách. Z~tohoto důvodu jsou metody umělé inteligence vhodnější spíše pro vylepšení klíčových částí těchto konvenčních algoritmů, než pro přímé řešení problému \cite{41} \cite{42} \cite{43} \cite{44}.

\section{Metody prohledávání stavového prostoru}
V~SFM metodách, které jsou vzhledem ke stanoveným požadavkům nejvhodnějším kandidátem pro řešení dané úlohy, je vždy nutně jeden krok minimalizující chybu určení pohybu kamery. Vstupem pro tuto minimalizaci jsou zpravidla mnoha dimenzionální data. Obvykle se jedná posuv a rotaci kamery. Tyto informace mívají (dle použitého kódování) 6 a více stupňů volnosti. Výstupem pak bývá přímo velikost chyby. Jedná se optimalizační úlohu, kterou je možné řešit některou metodou z~oblasti prohledávání stavového prostoru.

Prohledávání stavového prostoru je dobře zavedená podmnožina metod umělé inteligence. Dělí se dále na:

\begin{itemize} 
\item Neinformované metody
\item Informované metody
\end{itemize}

Neinformované metody nemají k~dispozici žádné dodatečné informace o~prostoru, který procházejí systematicky. Vzhledem k~vysoké dimensionalitě vstupních parametrů nepřipadají tyto metody příliš v~úvahu z~důvodu časové náročnosti. Při jejich použití by nejspíše bylo možné využít prostorovou lokalitu kamery. Lze předpokládat, že kamera se od původní pozice nepohne příliš daleko, čímž by bylo možné tyto algoritmy omezit a prohledávat neinformovaně pouze malou část prostoru. Přestože tato skupina metod není vhodná pro řešení tohoto problému, může poskytnout užitečné informace o~podobě prohledávaného stavového prostoru.

Informované metody mají znalost o~stavovém prostoru, kterou využívají při hledání optimálního řešení. Tato znalost se nazývá heuristika. Na kvalitě heuristiky přímo závisí výsledky, kterých metody dosahují.

\subsection{Gradientní algoritmus}
Gradientní algoritmus je nejjednodušší informovaná metoda. Algoritmus je obvykle inicializovaný v~náhodném bodě stavového prostoru. Následně je vygenerována množina podobných bodů. Z~tohoto okolí je dle heuristiky vybrán nejlepší nový bod. Pro nový bod se hledání opakuje, dokud nejsou všechny sousední body horší než bod aktuální.

Jedná se o~jednoduchý iterativní algoritmus, který ovšem nalezne pouze lokální optimum, nemá-li prostor konvexní charakter. Pro zvýšení pravděpodobnosti nalezení globálního optima bývá často algoritmus spouštěn vícekrát s~náhodou inicializací \cite{hillClimbing}.

\subsection{Simulované žíhání}
Simulované žíhání je optimalizační úloha prohledávání stavového prostoru. Své jméno získala od žíhání kovu, které simuluje. Snaží se řešit problém uvíznutí v~lokálním optimu kontrolou velikosti kroku a možností přijmout i zhoršující řešení. Po celou dobu běhu se pracuje pouze s~jedním kandidátním řešením.

Velikost kroku i pravděpodobnost přijetí zhoršujícího se řešení jsou závislé na aktuální teplotě. Ta je na začátku běhu nastavena na vysokou hodnotu a následně snižována na základě určené rychlosti konvergence. Vysoká teplota podporuje únik od lokálního atraktoru, zatímco nízká teplota podporuje konvergenci \cite{simulatedAnnealing}.

\subsection{Genetický algoritmus}
\Gls{GA} je metoda prohledávání stavového prostoru, která se řadí do skupiny evolučních algoritmů (algoritmy inspirované evolucí v~přírodě). Používá se pro získání relativně kvalitních řešení problémů, pro které obvykle neexistuje exaktní řešení. GA pracuje s~celou populací potenciálních řešení. Populace je následně skrze selekci křížení a mutaci upravována tak, aby populace konvergovala k~optimálnímu řešení.

Každý genetický algoritmus se skládá z~těchto dílčích částí:

\begin{enumerate}
\item Vytvoření počáteční populace
\item Selekce jedinců do nové generace
\item Volitelně výběr elitních jedinců
\item Křížení
\item Mutace
\item Výpočet zdatnosti jedinců dle zadané heuristické funkce
\item Opakování selekce (bod 2), pokud není splněna ukončovací podmínka
\item Jedinec s~nejvyšší zdatností je použit jako výstup algoritmu
\end{enumerate}


Každý jedinec je jednoznačně definován svým genotypem. Počáteční populace je obvykle vytvořena náhodným generováním genotypů. Existuje-li odhad vhodného kandidáta na optimální řešení, je možné tyto kandidáty zahrnout do počáteční populace. Je však třeba pamatovat na to, že tím algoritmus přinutíme nejprve prohledávat jistou oblast stavového prostoru, což bude mít vliv na dobu, kterou bude algoritmus potřebovat k~nalezení optimálního řešení. Pokud bude odhad správný, doba konvergence bude kratší, bude-li však nesprávný, algoritmus může konvergovat déle.

Selekce plní v~GA důležitou roli výběru zdatnějších jedinců. Díky selekci celá populace postupně směřuje k~lepším řešením. Míra preference zdatnějších řešení se nazývá selekční tlak. Metody selekce s~vyšším selekčním tlakem budou rychleji konvergovat, ale budou se obtížněji a déle dostávat z~lokálních extrémů. Naopak nízký selekční tlak umožní volnější prohledávání, ale metoda se bude více blížit prohledávání náhodnému a bude konvergovat déle.

Jednou z~nejčastěji užívaných technik selekce je takzvaná vážená ruleta. Každý jedinec získá pravděpodobnost výběru poměrně k~jeho zdatnosti. Jedinci s~vysokou zdatností tak mají vyšší pravděpodobnost podílet se na tvorbě další generace. Pravděpodobnost výběru je znázorněna vztahem \ref{V:selectionP}.

Používá se také turnajová metoda, kde jsou jedinci nejprve náhodně vybráni do skupiny fixní velikosti a následně je z~této skupiny vybrán jeden s~nejvyšší zdatností. Tento výběr se opakuje, dokud není v~nové populaci dostatek jedinců.

Je-li vyžadována jednoduchost a rychlost, prosté oříznutí špatných jedinců bude také fungovat. Ostatní metody však dovolí některým méně zdatným jedincům vydržet v~populaci déle a dají jim prostor se zlepšit. Z~toho plyne, že tato jednoduchá metoda bude o~něco hůře opouštět lokální optima.

\gr[.8]{res/genAlgorithm}{G:genAlgorithm}{Schéma genetického algoritmu}

Jedinci, kteří projdou selekcí, jsou následně kříženy. Proces křížení je vytváření jednoho nebo více jedinců na základě genotypu dvou jiných jedinců (rodičů). Cílem je kombinací dvou genotypů úspěšných jedinců vytvořit jedince s~vyšší zdatností. Vhodný způsob křížení závisí na způsobu, jakým je genotyp zakódován, a jeho sémantice. Obvykle se používá $N$-bodové křížení, kde $N$ je nízké číslo (například jedna nebo dva). V~takovém případě je genotyp rozdělen do $N$ částí. Nový jedinec vznikne tak, že přebírá jednotlivé částí od svých rodičů. Vždy sudé (resp. liché) části od jednoho rodiče a liché (resp. sudé) od rodiče druhého. Obvykle se volí nízká čísla dělení, protože se předpokládá, že sousední informace v~genotypu spolu nějak souvisí a kvalita jedince je odvislá od nějakého většího kusu genotypu, který by měl být přenesen do potomka jako celek. Hranice zvolených $N$ částí jsou voleny náhodně. V~některých případech může být užitečná i technika rovnoměrného křížení, kdy se $N$ nastaví na velikost genotypu a pro každou část se náhodně volí, ze kterého rodiče bude převzata.

Mutace zabraňuje předčasné konvergenci a umožňuje náhodné prohledávání blízkého okolí jedinců. Díky tomu je možné získat jedince s~novými, unikátními vlastnostmi. Snižuje selekční tlak a skrze náhodné změny pomáhá populaci dostat se z~lokálního optima. V~závislosti na konkrétním řešeném problému je míru mutace možné měnit i na základě momentální míry konvergence populace. Pravděpodobnost toho, že u~jedince dojde k~mutaci, se obvykle nastavuje na velmi nízkou hodnotu. Obvykle se jedná o~jednotky procent nebo méně. Výjimkou mohou tvořit malé populace, kde může být vhodné nastavit pravděpodobnost mutace vyšší. Vysoké hodnoty mutace však algoritmus posouvají blíže náhodnému prohledávání. Během mutace je náhodně změněna náhodná část genotypu.

\begin{align}
p_i=\dfrac{f_i}{\sum_{1}^{N}{f_i}}
\label{V:selectionP}
\end{align}

\begin{itemize}
	\item []{$P_i$} pravděpodobnost výběru i-tého jedince
	\item []{$f_i$} zdatnost i-tého jedince
	\item []{$N$} počet jedinců v~populaci
\end{itemize}

V~populaci je také možné ponechat některé dobré jedince beze změn. Tím je zaručeno, že nejlepší doposud nalezená řešení budou vždy v~populaci přítomná a mohou být použita k~vytváření nových jedinců skrze křížení a mutaci. Toto chování populace se nazývá elitismus.

Nová generace následně nahradí tu původní. Způsob, kterým se toto nahrazení provede, je silně implementačně závislý. Je možné vytvořit obě populace zcela nezávislé, kdy je nová populace složena pouze z~potomků a elitních jedinců. Alternativně je možné například nahrazovat jedince s~nízkou zdatností, nebo jedince, kteří jsou nejvíce podobní těm, které se chystají je nahradit.

Ukončovací podmínka je opět volena podle sémantiky problému. Cyklus může být například ukončen, jakmile je nalezeno dostatečně kvalitní řešení, algoritmus běží již příliš dlouho nebo pokud se již nějakou dobu nezměnilo nejlepší řešení. Po splnění ukončovací podmínky je následně z~populace vybrán jedinec s~nejvyšší zdatností \cite{genAlg}.

\section{Metody zpracování obrazu}
SFM metody také obsahují různorodá zpracování 2D snímků. V~prvních fázích se jedná zejména o~různá předzpracování obrazu a extrakci vlastností z~obrazu. V~těch pozdějších pak například o~výpočet chyby mezi vzájemně perspektivně zkreslenými extrahovanými vlastnostmi.

Zpracování obrazu je další velkou podoblastí metod umělé inteligence. Často přímo souvisí s~dobýváním znalostí z~dat, nebo strojovým učením. Typickými problémy, které jsou úspěšně řešeny metodami umělé inteligence, jsou například:

\begin{itemize}
\item Klasifikace
\item Segmentace a detekce objektů
\item Generování bitmapy na základě abstraktních parametrů
\end{itemize}

Pro řešení těchto problému se velice často používají neuronové sítě, které v~mnoha případech předčí doposud používané konvenční metody.

\subsection{Neuronové sítě}
Umělé neuronové sítě jsou výpočetním modelem založeným na spojení mnoha jednoduchých výpočetních elementů, který je inspirován propojením neuronů v~nervové soustavě živočichů. Jedním z~nejjednodušších výpočetních modelů neuronu je perceptron, který je znázorněn na obrázku \ref{G:perceptron}. Výstup každého neuronu je získán součtem vážených vstupů, který je následně upraven aktivační funkcí viz. vztah \ref{V:perceptron}. Například perceptron, jehož aktivační funkce má striktně binární výstup, tak dokáže vstupní prostor rozdělit na dvě části \cite{NN}.
\begin{align}
 y=f_a\left(w_0 + \sum_{1}^{N}{\left(x_i * w_i\right)}\right)
 \label{V:perceptron}
\end{align}
\begin{itemize}
	\item []{$w_i$} váha $i$-tého vstupu
	\item []{$x_i$} $i$-tý vstup
	\item []{$w_0$} práh neuronu
	\item []{$f_a$} aktivační funkce
		\item []{$N$} dimenze vstupního prostoru (počet vstupů)
\end{itemize}

\grc[.8]{res/perceptron}{G:perceptron}{Princip evaluace a učení jediného perceptronu}{NN}

Tyto základní výpočetní elementy se spojují do neuronových sítí tak, aby získaly schopnost naučit se komplikovanější vzory. Při práci s~bitmapou se velice často používá uspořádání neuronů do takzvaných konvolučních sítí. V~tomto případě je vstup každého neuronu omezen na relativně malé okolí jednoho neuronu předchozí vrstvy (popřípadě na okolí jednoho obrazového bodu v~případě vstupní vrstvy). Tím je oproti plně propojeným sítím výrazným způsobem omezen počet parametrů, které je třeba učit. Výpočetně náročný proces učení je takto urychlen a dovoluje použití rozsáhlejších sítí. Struktura možné konvoluční sítě pro klasifikaci snímku je znázorněna na obrázku \ref{G:CNNSchema}.

\gr{res/CNNSchema}{G:CNNSchema}{Možná struktura konvoluční neuronové sítě pro jednoduchý klasifikátor}

Tyto sítě je možné nakonfigurovat i takovým způsobem, aby fungovaly opačně. V~takovém případě je výstupem sítě snímek a vstupem data o~relativně nízké dimenzi. Tato data charakterizují určitě vlastnosti výsledného obrázku, které se postupně dekonvolucí propagují do výsledného snímku. Takovéto sítě se nazývají generativní konvoluční sítě.

Pokud se oba přístupy spojí, je možné vytvořit generativní konvoluční síť, která je schopná naučit se konkrétní transformaci obrázku, je-li k~dispozici dostatečné množství trénovacích dat.

Publikace \cite{45}, \cite{46} a \cite{47} ukazují, že generativní konvoluční neuronové sítě mohou fungovat i jako detektory klíčových bodů. Ukazuje se, že neuronové sítě jsou schopné se naučit i moderní detektory klíčových bodů, jakými jsou například SIFT, MSER, SURF, nebo KAZE.
Je-li takový detektor učen konvenčním algoritmem s~vyšší časovou náročností, je navíc možné dosahovat mnohem vyšších rychlostí evaluace za cenu relativně náročného trénování konkrétního detektoru. Protože většina SFM metod používá nějakou formu takového detektoru, jsou konvoluční neuronové sítě potencionálním prostředkem urychlení těchto metod.